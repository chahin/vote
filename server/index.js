const express = require("express");
const cors = require('cors')
const app = express();

app.use(cors());

let yes = 0;
let no  = 0;

app.put("/yes", (req, res) => {
  yes++;
  console.log(`yes: ${yes}`);
});

app.put("/no", (req, res) => {
  no++;
  console.log(`no: ${no}`);
});

app.get("/yes", (req, res) => {
  res.status(200).send(yes.toString());
});

app.get("/no", (req, res) => {
  res.status(200).send(no.toString());
});

app.listen(3000, () => console.log("listening on port 3000..."));